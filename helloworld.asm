; Programa "Hello World"
;
; author: AlphaLawless

section .text
global _start

_start:
    mov edx,len                   ;comprimento da mensagem
    mov ecx,msg                   ;mensagem a ser escrita
    mov ebx,1                     ;descritor de arquivo(stdout) - Vou fazer com comentário @Matan#9968
    mov eax,4                     ;número da system call - SIM!

; (sys_write) - Não sei assembly!
    int 0x80                      ;call kernel - Isso vai chamar o meu kernel

    mov eax,1                     ;número da syscall

; (sys_exit)
    int 0x80                      ;call kernel

section .data
    msg db 'Hello, world!',0xa    ;nossa string "Hello World"
    len equ $ - msg               ;comprimento da string

; P.S.: Eu não sei se vai funcionar, porque eu to fazendo usando tab 4, e acredito que preciso do tab 6, qualquer coisa
; eu mudo para o vim
